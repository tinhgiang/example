package com.example.model;

import com.example.entity.Product;
import com.example.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProductModel {

    private static Logger LOGGER = Logger.getLogger(ProductModel.class.getSimpleName());

    public List<Product> findAll() {
        List<Product> products = new ArrayList<>();
        try (Session session = HibernateUtil.getSession()) {
            products = session.createQuery("from Product", Product.class).list();
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, String.format("Can not findAll product, stack trace"), e);
        }
        return products;
    }

    public Product loadProduct(String id) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSession()) {
            transaction = session.beginTransaction();
            Product product = session.load(Product.class, id);
            transaction.commit();
            LOGGER.log(Level.INFO,String.format("Load product success with rollNumber %s", id));
            return product;
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.log(Level.SEVERE,String.format("Load product error , stack trace ", e));
            return null;
        }

    }
}
