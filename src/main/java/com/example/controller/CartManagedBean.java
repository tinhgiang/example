package com.example.controller;

import com.example.entity.Item;
import com.example.entity.Product;

import javax.faces.bean.ManagedBean;
import java.util.ArrayList;
import java.util.List;

@ManagedBean(name = "cartController")
public class CartManagedBean {

    private List<Item> items;
    double totalAmount = 0;

    public CartManagedBean() {
        this.items = new ArrayList<Item>();
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public String buy(Product product) {
        int index = this.exists(product);
        if (index == -1) {
            this.items.add(new Item(product, 1));
        } else {
            int quantity = this.items.get(index).getQuantity() + 1;
            this.items.get(index).setQuantity(quantity);
        }
        return "cart?faces-redirect=true";
    }

    public double total() {
        for (Item item : this.items) {
            totalAmount += item.getProduct().getPrice() * item.getQuantity();
        }
        return totalAmount;
    }

    public double totalDiscount() {
        double totalDiscount = 0;

        if (totalAmount >= 5) {
            totalDiscount = totalAmount - ((totalAmount * 30) / 100);
        }
        return totalDiscount;
    }

    private int exists(Product product) {
        for (int i = 0; i < this.items.size(); i++) {
            if (this.items.get(i).getProduct().getId() == product.getId()) {
                return i;
            }
        }
        return -1;
    }



}
