package com.example.controller;

import com.example.entity.Product;
import com.example.model.ProductModel;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ManagedBean(name = "productManagedBean", eager = true)
@SessionScoped
public class ProductManagedBean {

    private String name;
    private String address;
    private String phone;

    private List<Product> products;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public ProductManagedBean() {
        ProductModel productModel = new ProductModel();
        this.products = productModel.findAll();
    }

    public String index() {
        ProductModel productModel = new ProductModel();
        this.products = productModel.findAll();
        return "index?faces-redirect=true";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
